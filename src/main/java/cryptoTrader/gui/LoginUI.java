package cryptoTrader.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Scanner;

class LoginFrame extends JFrame implements ActionListener {
	JPanel newPanel;
	JLabel usernameInput, passwordInput;
	JButton submit;
	JTextField userText, passText;

	LoginFrame() {
		usernameInput = new JLabel();
		usernameInput.setText("Username"); 
		userText = new JTextField(); 
		
		passwordInput = new JLabel();
		passwordInput.setText("Password"); 
		passText = new JPasswordField(); 
		
		submit = new JButton("Submit"); 

		// Add username, password inputs and submit button components to panel
		newPanel = new JPanel(new GridLayout(3, 1));
		newPanel.add(usernameInput); 
		newPanel.add(userText); 
		newPanel.add(passwordInput); 
		newPanel.add(passText); 
		newPanel.add(submit); 
		add(newPanel, BorderLayout.CENTER);  
		submit.addActionListener(this); 
	}

	public void actionPerformed(ActionEvent ae) {
		String userValue = userText.getText(); 
		String passValue = passText.getText(); 
		
		Hashtable<String, String> userPass = new Hashtable<String, String>();
		try {
	      File myObj = new File("UsernamePassword.txt");
	      Scanner myReader = new Scanner(myObj);
	      while (myReader.hasNextLine()) {
	        String data = myReader.nextLine();
	        String[] words = data.split(",");
	        userPass.put(words[0].strip(), words[1].strip());
	      }
	      myReader.close();
	    } catch (FileNotFoundException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }

		if (userPass.containsKey(userValue) && userPass.get(userValue).equals(passValue)) { 
			this.setVisible(false);
			JFrame frame = MainUI.getInstance();
			frame.setSize(1200, 600);
			frame.pack(); 
			frame.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(null, "Invalid username and password.");
			System.exit(0);
		}
	}
}

class LoginUI {
	public static void main(String args[]) {
		LoginFrame form = new LoginFrame();
		form.setSize(300,100); 
		form.setVisible(true); 
	}
}