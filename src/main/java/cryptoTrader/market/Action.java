package cryptoTrader.market;

import java.time.LocalDate;

public class Action {
	private String broker;
	private String strategy;
	private String crypto;
	private String type;
	private int amount;
	private double price;
	private String date;
	
	/**
	 * Constructor method
	 * Calculates and sets date parameter internally
	 * @param broker broker that performed action
	 * @param strategy strategy executed
	 * @param crypto crypto bought/sold
	 * @param type buy or sell
	 * @param amount amount of crypto exchanged
	 * @param price price of crypto exchanged
	 */
	public Action(String broker, String strategy, String crypto, String type, int amount, double price) {
		this.broker = broker;
		this.strategy = strategy;
		this.crypto = crypto;
		this.type = type;
		this.amount = amount;
		this.price = price;
		
		LocalDate today = LocalDate.now();
		this.date = today.toString(); // TODO formatting
	}
	
	/**
	 * Getter method for broker name
	 * @return broker name
	 */
	public String getBroker() {
		return broker;
	}
	
	/**
	 * getter method for strategy name
	 * @return strategy name
	 */
	public String getStrategy() {
		return strategy;
	}
	
	/**
	 * getter method for crypto name
	 * @return crypto name
	 */
	public String getCrypto() {
		return crypto;
	}
	
	/**
	 * getter method for transaction type
	 * @return buy or sell
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * getter method for amount of crypto exchanged
	 * @return amount exchanged
	 */
	public String getAmount() {
		return ((Integer)amount).toString();
	}
	
	/**
	 * getter method for price of crypto
	 * @return price of crypto
	 */
	public String getPrice() {
		return ((Double)price).toString();
	}
	
	/**
	 * getter method for date of action
	 * @return date of action
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * Represents the action as an array
	 * the array is used to display the action history
	 * @return array containing all parameters
	 */
	public String[] getArray() {
		String[] output = new String[7];
		output[0] = broker;
		output[1] = strategy;
		output[2] = crypto;
		output[3] = type;
		output[4] = ((Integer)amount).toString();
		output[5] = ((Double)price).toString();
		output[6] = date;
		return output;
	}
}
