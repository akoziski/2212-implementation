package cryptoTrader.market;

import java.util.*;

public class BrokerRoster {
	private ArrayList<Broker> roster;
	
	/**
	 * Constructor
	 * Creates one broker object because the UI always starts with one row on the broker panel
	 */
	public BrokerRoster() {
		roster = new ArrayList<Broker>();
		roster.add(new Broker());  // THIS IS VERY IMPORTANT
	}
	
	/**
	 * Searches for and returns a broker by name
	 * @param name name of broker
	 * @return requested broker object or null if the name isn't found
	 */
	public Broker getBroker(String name) {
		for (int i = 0; i < roster.size(); i++) {
			if (roster.get(i).getName().equals(name)) return roster.get(i);
		}
		return null;
	}
	
	/**
	 * overloaded method to retrieve a broker by it's position in the ArrayList
	 * @param index position of broker in arraylist
	 * @return requested broker object or null if the name isn't found
	 */
	public Broker getBroker(int index) {
		if (index < roster.size() && index >= 0) return roster.get(index);
		else return null;
	}
	
	/**
	 * Adds an empty broker to the roster
	 */
	public void addBroker() {
		Broker newBroker = new Broker();
		roster.add(newBroker);
	}
	
	/**
	 * removes a broker based on position in roster
	 * @param index position of broker in roster
	 */
	public void removeBroker(int index) {
		roster.remove(index);
	}
	
	/**
	 * checks if a broker name is present within the roster
	 * @param name name to check
	 * @return true if found, false if not
	 */
	public boolean containsBroker(String name) {
		for (int i = 0; i < roster.size(); i++) {
			if (roster.get(i).getName().equals(name)) return true;
		}
		return false;
	}
	
	/**
	 * sets the name of the broker at index
	 * @param index position in roster
	 * @param name new name
	 */
	public void setName(int index, String name) {
		if (!containsBroker(name)) {
			roster.get(index).setName(name);
		}
	}
	
	/**
	 * sets the strategy of the broker at index
	 * @param index position in roster
	 * @param name new strategy name
	 */
	public void setStrategy(int index, String name) {
		roster.get(index).setStrategy(name);
	}
	
	/**
	 * sets crypto interest of the broker at index
	 * @param index position in roster
	 * @param interests new array of cryptocurrencies to receive updates about
	 */
	public void setInterest(int index, String[] interests) {
		roster.get(index).setInterest(interests);
	}
	
	/**
	 * gets the number of brokers int he roster
	 * @return size of arraylist
	 */
	public int getSize() {
		return roster.size();
	}
	
	/**
	 * Represents the roster as a String
	 * useful for testing and troubleshooting
	 */
	public String toString() {
		String output = "";
		for (int i = 0; i < roster.size(); i++) {
			output += "[";
			output += roster.get(i);
			output += ", ";
			output += roster.get(i).getStrategy();
			output += "], ";
		}
		return output;
	}
}
