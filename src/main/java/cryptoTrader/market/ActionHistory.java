package cryptoTrader.market;

import java.util.ArrayList;

public class ActionHistory {
	private ArrayList<Action> history;
	
	/**
	 * Constructor: creates an empty history
	 */
	public ActionHistory() {
		history = new ArrayList<Action>();
	}
	
	/**
	 * Adds an action at the end of the history
	 * Note that a remove function is unnecessary
	 * @param newAction action to be added
	 */
	public void addAction(Action newAction) {
		if (newAction != null) history.add(newAction);
	}
	
	/**
	 * Returns the string array representation of an action
	 * @param index position of action in history
	 * @return string array representing action
	 */
	public String[] getAction(int index) {
		return history.get(index).getArray();
	}
	
	/**
	 * returns number of actions in history
	 * @return size of arraylist
	 */
	public int getSize() {
		return history.size();
	}
}
