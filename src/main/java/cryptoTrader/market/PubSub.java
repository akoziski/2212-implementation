package cryptoTrader.market;

import java.time.LocalDate;
import java.util.ArrayList;
import cryptoTrader.utils.DataFetcher;

public class PubSub {
	private DataFetcher fetcher;
	private ArrayList<Cryptocoin> cryptos;
	
	/**
	 * Constructor
	 */
	public PubSub() {
		fetcher = new DataFetcher();
		cryptos = new ArrayList<Cryptocoin>();
	}
	
	/**
	 * Adds a singular cryptocurrency with name specified by id to the ArrayList if it isn't already present 
	 * @param id
	 */
	public void addCrypto(String id) {
		for (int i = 0; i < cryptos.size(); i++) {
			if (cryptos.get(i).toString().equals(id)) return;
		}
		cryptos.add(new Cryptocoin(id));
	}
	
	/**
	 * iteratively adds multiple cryptocurrencies to the ArrayList
	 * @param ids
	 */
	public void addCryptos(String[] ids) {
		for (int j = 0; j < ids.length; j++) {
			this.addCrypto(ids[j]);
		}
	}

	/**
	 * Fetches cryptocurrency prices from the CoinGecko API and notifies brokers with the prices that they are interested in
	 * @param subs BrokerRoster containing all active trading brokers
	 * @return ArrayList of actions taken after the refresh
	 */
	public ArrayList<Action> fetch(BrokerRoster subs) {
		LocalDate today = LocalDate.now();
		
		String date = ((Integer)today.getDayOfMonth()).toString();
		date += "-";
		date += ((Integer)today.getMonthValue()).toString();
		date += "-";
		date += ((Integer)today.getYear()).toString();
		
		ArrayList<Action> taken = new ArrayList<Action>();
		
		for (int i = 0; i < cryptos.size(); i++) {
			if (cryptos.get(i).getName().equals("btc") || cryptos.get(i).getName().equals("BTC")) {
				cryptos.get(i).setPrice(fetcher.getPriceForCoin("bitcoin", date));
			}
			else if (cryptos.get(i).getName().equals("eth") || cryptos.get(i).getName().equals("ETH")) {
				cryptos.get(i).setPrice(fetcher.getPriceForCoin("ethereum", date));
			}
			else if (cryptos.get(i).getName().equals("ada") || cryptos.get(i).getName().equals("ADA")) {
				cryptos.get(i).setPrice(fetcher.getPriceForCoin("cardano", date));
			}
			else {
				cryptos.get(i).setPrice(fetcher.getPriceForCoin(cryptos.get(i).getName(), date));
			}
		}
		
		for (int j = 0; j < subs.getSize(); j++) {
			int index = 0;
			String[] interests = subs.getBroker(j).getInterest();
			String[] outputNames = new String[interests.length];
			double[] outputPrices = new double[interests.length];
			for (int k = 0; k < cryptos.size(); k++) {
				for (int l = 0; l < interests.length; l++) {
					if (cryptos.get(k).getName().equals(interests[l])) {
						outputNames[index] = cryptos.get(k).getName();
						outputPrices[index] = cryptos.get(k).getPrice();
						index++;
					}
				}
			}
			taken.add(subs.getBroker(j).notifyPrices(outputNames, outputPrices));
		}
		
		return taken;
	}
}
