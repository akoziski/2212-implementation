package cryptoTrader.market;

public class Broker {
	private String name;
	private Strategy strategy;
	private String[] interest;
	
	/**
	 * Constructor: creates an empty broker
	 */
	public Broker() {
		name = "";
		strategy = new Strategy();
		interest = new String[0];
	}
	
	/**
	 * Getter method for name
	 * @return broker name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter method for strategy
	 * @return strategy name
	 */
	public String getStrategy() {
		return strategy.getName();
	}
	
	/**
	 * getter method for cryptocurrencies that broker is interested in
	 * @return string array containing crypto names
	 */
	public String[] getInterest() {
		return interest;
	}
	
	/**
	 * name setter method
	 * @param name new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * strategy setter method
	 * @param strategyName new strategy name
	 */
	public void setStrategy(String strategyName) {
		strategy.setName(strategyName);
	}
	
	/**
	 * interest setter method
	 * @param interest new string array of interests
	 */
	public void setInterest(String[] interest) {
		this.interest = interest;
	}
	
	/**
	 * receives two arrays: one of desired crypto names and one with their respective prices
	 * these arrays are passed to the strategy returns an Action object if action is taken
	 * the Action object is passed through the broker 
	 * @param names cryptocurrency names
	 * @param cryptoPrices respective cryptocurrency prices
	 * @return action object if trading occurred. returns null if strategy does not execute
	 */
	public Action notifyPrices(String[] names, double[] cryptoPrices) {
		return strategy.execute(name, names, cryptoPrices);
	}

	public String toString() {
		return name;
	}
}
