package cryptoTrader.market;

public class Cryptocoin {
	private String name;
	private double price;
	
	/**
	 * Constructor
	 * @param name name of coin 
	 */
	public Cryptocoin(String name) {
		this.name = name;
		this.price = -1.0;
	}
	
	/**
	 * Overloaded constructor to allow for creation of coin and setting of price simeltaneously
	 * @param name name of coin
	 * @param price value of coin
	 */
	public Cryptocoin(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	/**
	 * name getter method
	 * @return coin name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * price getter method
	 * @return coin price
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * price setter method
	 * @param newPrice new price
	 */
	public void setPrice(double newPrice) {
		price = newPrice;
	}

	public String toString() {
		return name;
	}
}
