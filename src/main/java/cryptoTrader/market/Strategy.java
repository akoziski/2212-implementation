package cryptoTrader.market;

import cryptoTrader.market.Action;

// TODO: refactor as singleton (?)
// will require changes in Broker.java strategy operations

public class Strategy {
	private String name;
	
	/**
	 * Constructor
	 * The strategy's name controls behaviour when it is executed
	 * 
	 * Strategy-A: if BTC < 60,000 buy 100 BTC
	 * Strategy-B: if ETH > 2,000 sell 50 ETH
	 * Strategy-C: if ADA < BTC buy 250 ADA
	 * Strategy-D: if ETH < 5,000 && BTC > 40,000 sell 500 ADA
	 */
	public Strategy() {
		name = "";
	}
	
	/**
	 * Getter method for name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter method for name
	 * @param name new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Extracts relevant crypto prices from input
	 * @param brokerName name of broker invoking the method
	 * @param names names of cryptocurrencies
	 * @param prices respective prives of cryptocurrencies
	 * @return Action object detailing trading or null if nothing happens
	 */
	public Action execute(String brokerName, String[] names, double[] prices) {
		double BTCPrice = -1.0;
		double ETHPrice = -1.0;
		double ADAPrice = -1.0;
		
		for (int i = 0; i < names.length; i++) {
			if (names[i].equals("BTC") || names[i].equals("btc") || names[i].equals("bitcoin")) BTCPrice = prices[i];
			if (names[i].equals("ETH") || names[i].equals("eth") || names[i].equals("ethereum")) ETHPrice = prices[i];
			if (names[i].equals("ADA") || names[i].equals("ada") || names[i].equals("cardano")) ADAPrice = prices[i];
		}
		
		if (name.equals("Strategy-A") && BTCPrice != -1.0) {
			if (BTCPrice < 60000) {
				// buy 100 BTC
				return new Action(brokerName, name, "BTC", "Buy", 100, BTCPrice);
			}
		}
		else if (name.equals("Strategy-B") && ETHPrice != -1.0) {
			if (ETHPrice > 2000) {
				// sell 50 ETH
				return new Action(brokerName, name, "ETH", "Sell", 50, ETHPrice);
			}
		}
		else if (name.equals("Strategy-C") && BTCPrice != -1.0 && ADAPrice != -1.0) {
			if (ADAPrice < BTCPrice) {
				// Buy 250 ADA
				return new Action(brokerName, name, "ADA", "Buy", 250, ADAPrice);
			}
		}
		else if (name.equals("Strategy-D") && BTCPrice != -1.0 && ETHPrice != -1.0  && ADAPrice != -1.0) {
			if (ETHPrice < 5000 && BTCPrice > 40000) {
				// sell 500 ADA
				return new Action(brokerName, name, "ADA", "Sell", 500, ADAPrice);
			}
		}
		return null;
	}
}
